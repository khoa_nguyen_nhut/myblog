﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyBlog.Data;
using MyBlog.Models;

namespace MyBlog.Controllers
{
    public class AccountController : Controller
    {
        private readonly MyBlogContext _blogContext;

        public AccountController(MyBlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login(string Username, string Password)
        {
            var user = _blogContext.Users.FirstOrDefault(u => u.Username.Equals(Username) && u.Password == Password);
            if (user != null)
            {
                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Role, user.Role),
                }, CookieAuthenticationDefaults.AuthenticationScheme); ;

                var principal = new ClaimsPrincipal(identity);

                var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            }
            return RedirectToAction("Index","Home");
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult SubmitRegister(UserModel user)
        {
            try
            {
                _blogContext.Add(user);
                _blogContext.SaveChanges();
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
            
            return RedirectToAction("Index", "Home");
        }
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View("~/Views/Post/Register.cshtml"); 
        }
        [AllowAnonymous]
        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }
    }
}