﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyBlog.Data;
using MyBlog.Models;

namespace MyBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly MyBlogContext _blogContext;

        public HomeController(MyBlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            if (_blogContext.Database.EnsureCreated())
            {
                foreach (var item in Data.Data.Init())
                {
                    _blogContext.Add(item);
                    _blogContext.SaveChanges();
                }
            }
            return View(_blogContext.Posts.OrderByDescending(Post => Post.Date).ToList());
        }
        [AllowAnonymous]
        public IActionResult Search(string search)
        {
            if (search == null)
            {
                return View("Index", _blogContext.Posts.ToList());
            }
            var post = _blogContext.Posts.Where(p => p.Title.Contains(search)).OrderByDescending(Post => Post.Date).ToList();
            return View("Index", post);
        }

    }
}
