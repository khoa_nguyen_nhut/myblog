﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyBlog.Data;
using Microsoft.EntityFrameworkCore;
using MyBlog.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace MyBlog.Controllers
{
    public class PostController : Controller
    {
        private readonly IWebHostEnvironment _environment;
        private readonly MyBlogContext _blogContext;
        public PostController(IWebHostEnvironment environment, MyBlogContext blogContext)
        {
            _environment = environment;
            _blogContext = blogContext;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ViewPost(int id, MutipleModel model)
        {
            if (id <= 0)
            {
                id = 1;
            }
            try
            {
                var Post = await _blogContext.Posts.Include(post => post.Comments).FirstOrDefaultAsync(m => m.ID == id);
                model.Post = Post;
            }
            catch (Exception)
            {

                throw;
            }
            if (model.Post == null)
            {
                return Redirect("~/Post/ViewPost/" + (id - 1));
            }
            return View(model);
        }
        [HttpGet]
        [Authorize(Roles = "admin, user")]
        public IActionResult CreatePost()
        {
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "admin, user")]
        public IActionResult SubmitPost(PostInputModel model)
        {
            string filename = null;
            if (model.Image != null)
            {
                string uploadsFolder = Path.Combine(_environment.WebRootPath, "BlogImages");
                string filePath = Path.Combine(uploadsFolder, model.Image.FileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                model.Image.CopyTo(fileStream);
                filename = model.Image.FileName;
            }
            else
            {
                filename = "m-farmerboy.jpg";
            }
            PostModel Post = new PostModel()
            {
                Username = model.Username,
                Title = model.Title,
                Image = filename,
                Genre = model.Genre,
                Date = DateTime.Now,
                Summary = model.Summary,
                Content = model.Content,
                Comments = model.Comments
            };
            _blogContext.Add(Post);
            _blogContext.SaveChanges();
            return View("~/Views/Home/Index.cshtml", _blogContext.Posts.OrderByDescending(Post => Post.Date).ToList());
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SubmitComment(int id, MutipleModel model)
        {
            if (model == null)
            {
                return NotFound();
            }
            model.Comment.Date = DateTime.Now;
            model.Post = await _blogContext.Posts.Include(post => post.Comments).FirstOrDefaultAsync(m => m.ID == id);
            model.Post.Comments.Add(model.Comment);
            await _blogContext.SaveChangesAsync();
            return View("ViewPost", model);
        }
        [AllowAnonymous]
        public IActionResult AboutMe()
        {
            return View();
        }
    }
}